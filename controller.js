const userController = {}

const Pool = require("pg").Pool;
const pool = new Pool({
    user: 'picarrotAdmin',
    host: '51.159.24.37',
    database: 'db_picarrot_users',
    password: 'e}KY8x>1Ad?sC_r:BG@n3',
    port: 36144,
})

userController.getUsers = async (req,res)=> {
    pool.query('SELECT * FROM "user"', (error, results) => {
        if (error) {
            res.status(400).send("erreur : " + error.message)
        }else{
            res.status(200).json(results.rows)
        }
    })
}

userController.getUser = async (req,res)=> {
    const id = req.params.id.toString()
    pool.query('SELECT * FROM "user" WHERE uuid_user = $1', [id], (error, results) => {
        if (error) {
            res.status(400).send("erreur : " + error.message)
        }else{
            res.status(200).json(results.rows)
        }
    })
}

userController.getUserKeycloak = async (req,res)=> {
    const id = req.params.id.toString()
    pool.query('SELECT * FROM "user" WHERE id_keycloak = $1', [id], (error, results) => {
        if (error) {
            res.status(400).send("erreur : " + error.message)
        }else{
            res.status(200).json(results.rows)
        }
    })
}

userController.getUserFromLogin = async (req,res)=> {
   const login = req.params.login.toString()
   pool.query('SELECT * FROM "user" WHERE user_login = $1', [login], (error, results) => {
        if (error) {
            res.status(400).send("erreur : " + error.message)
        }else{
            res.status(200).json(results.rows)
        }
   })
}
userController.createUser = async (req,res)=> {
    const {name, login, email, password, imageURL, location, birthDate, id_keycloak } = req.body

    pool.query('INSERT INTO "user"(user_name, user_login, user_email, id_keycloak) VALUES ($1, $2, $3, $4)',
    [name, login, email, id_keycloak], (error, results) => {
        if (error) {
            res.status(400).send("erreur : " + error.message)
        }else{
            res.status(201).send(`user added`)
        }
    })
}
userController.editUserLogin = async (req,res)=> {
    const { id, login } = req.body

    pool.query(
        'UPDATE "user" SET user_login = $1 WHERE uuid_user = $2',
        [login, id],
        (error, results) => {
            if (error) {
                res.status(400).send("erreur : " + error.message)
            }else{
                res.status(200).send(`User modified with ID: ${id}`)
            }
        }
    )
}
userController.editUser = async (req,res)=> {
    const { id, name, password, imageURL, location, birthDate, description, email } = req.body
    console.log("body : ", req.body)

    pool.query(
        'UPDATE "user" SET user_name = $1, user_password = $2, user_imageURL = $3, user_location = $4, user_birthDate = $5, user_description = $6, user_email = $7 WHERE uuid_user = $8',
        [name, password, imageURL, location, birthDate, description, email, id],
        (error, results) => {
            if (error) {
                res.status(400).send("erreur : " + error.message)
            }else{
                res.status(200).send(`User modified with ID: ${id}`)
            }
        }
    )
}
userController.removeUser = async (req,res)=> {
    const { id } = req.body

    pool.query('DELETE FROM "user" WHERE uuid_user = $1', [id], (error, results) => {
        if (error) {
            res.status(400).send("erreur : " + error.message)
        }else{
            res.status(200).send(`User deleted with ID: ${id}`)
        }
    })
}
// add optional param : User and Post
userController.countUsers = async (req,res)=> {
    pool.query('SELECT COUNT(*) FROM "user"', (error, results) => {
        if (error) {
            res.status(400).send("erreur : " + error.message)
        }else{
            res.status(200).json(results.rows)
        }
    })
}

module.exports = userController
