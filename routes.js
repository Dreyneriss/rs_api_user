const express = require('express')
const userController = require('./controller')
const router = express.Router()

router.get('/user/getUsers', userController.getUsers)
router.get('/user/getUsers/user/:id', userController.getUser)
router.get('/user/getUsers/keycloak/:id', userController.getUserKeycloak)
router.get('/user/getUsers/login/:login', userController.getUserFromLogin)
router.post('/user/addUser', userController.createUser)
router.put('/user/editUser', userController.editUser)
router.put('/user/editUser/login', userController.editUserLogin)
//router.patch('/users', userController.updateUser)
router.delete('/user/deleteUser',userController.removeUser)
router.get('/user/getUserCount',userController.countUsers)

module.exports = router
