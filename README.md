## Comprendre l'architecture

Les fichiers important sont :
- `app.js` : le point d'entrée de l'application,
- `routes.js` : la déclaration des routes,
- `Controllers/userController.js` : l'implémentation des routes,\
Ainsi que :
- Les fichiers `package` pour la configuration.

## Configuration des connexions

`app.js` contient le lieu de déployement de l'API, sous la forme :
```
// Start server and listen on http://localhost:8081/
var server = app.listen(8081, function () {
    var host = server.address().address
    var port = server.address().port

    console.log("app listening at http://%s:%s", "localhost", port)
});
```
Ici, l'API est déployée en localhost au port 8081.

`Controllers/userController.js` contient l'emplacement de la base de données, sous la forme :
```
const Pool = require("pg").Pool;
const pool = new Pool({
    user: 'picarrotAdmin',
    host: '51.159.24.37',
    database: 'db_picarrot_users',
    password: 'e}KY8x>1Ad?sC_r:BG@n3',
    port: 36144,
})
```
Ici, l'API se connecte à la base de données de notre serveur Scaleway avec les identifiants d'admin.

## Utiliser l'API

- Installez le projet via : `npm init`
- Lancez : `node app.js`\
 Ou
- Installez nodemon via : `npm install nodemon`
- Lancez nodemon : `nodemon`

Une fois l'application en route, allez sur http://localhost:8081/ dans votre navigateur et entrez une adresse voulue pour avoir une requête GET ce nécessitant pas de body :\
`http://localhost:8081/users`\
`http://localhost:8081/countUsers`

Pour chaque requêtes nécessitant d'entrer des données via un body, j’utilise Postman.

## Autres liens pratiques

https://tableplus.com/blog/2018/10/how-to-start-stop-restart-postgresql-server.html

Tuto pour mêler nodejs et postgresql :\
https://blog.logrocket.com/nodejs-expressjs-postgresql-crud-rest-api-example/

utiliser les uuid :\
https://www.postgresqltutorial.com/postgresql-uuid/ \
note : les uuid sont donc des strings et non des ints

utiliser les dates :\
https://usefulangle.com/post/187/nodejs-get-date-time \
https://www.postgresql.org/docs/9.5/datatype-datetime.html

Par contre, le dossier node_module aurait dû être ignoré, mais bon :/
